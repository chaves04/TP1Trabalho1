/** file: ball.cpp
 ** brief: Ball class - implementation
 ** author: Andrea Vedaldi
 **/

#include "ball.h"

#include <iostream>

Ball::Ball()
: r(0.1), x(0), y(0), vx(0.3), vy(-0.1), g(9.8), m(1),
xmin(-1), xmax(1), ymin(-1), ymax(1)
{ }

void Ball::step(double dt)
{
  double xp = x + vx * dt ;
  double yp = y + vy * dt - 0.5 * g * dt * dt ;
  if (xmin + r <= xp && xp <= xmax - r) {
    x = xp ;
  } else {
    vx = -vx ;
  }
  if (ymin + r <= yp && yp <= ymax - r) {
    y = yp ;
    vy = vy - g * dt ;
  } else {
    vy = -vy ;
  }
}

void Ball::display()
{
  std::cout<<x<<" "<<y<<std::endl ;
}

void Ball::setPosition(double x, double y)
{
	// Garante que o x estará dentro do dominio
	if (x + r > xmax)
		x = xmax - r;
	else if (x - r < xmin)
		x = xmin + r;

	// Garante que o y estará dentro do dominio
	if (y + r > ymax)
		y = ymax - r;
	else if (y - r < ymin)
		y = ymin + r;

	this->x = x;
	this->y = y;
}
 
// Apenas mostra a posição da bola na tela
void Ball::getPosition()
{
	std::cout << x << " " << y << std::endl;
}

// Passa a posição da bola para duas variávies, para que essa posição seja usada em outras partes do programa
void Ball::getPosition(double & x, double & y)
{
	x = this -> x;
	y = this -> y;
}