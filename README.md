# Trabalho 1 - Simulador de Física

### Introdução

O projéto é um simulador da física de um sístema de massas e molas.
Os objetivos do projéto é estudar a programação orientada à objeto por meio do feitio desse simulador, tal simulador de física é ideal para o estúdo pela facil associação de objetos virtuais com objetos concrétos.

### Requisítos 

Nesse projeto foi usada a linguagem de programação **C++ 98** (a versão padrão da maioria dos compiladores), o compílador usado foi o **g++** versão *6.3.0*.
Para que o projeto seja compialdo corretamente são necessários também os pacótes **freeglut3** e **freeglut3-dev** do *OpenGL*.
Além disso foi usado também o **GNU Make** versão *4.1*.

### Compilação

Como dito anteriormente, foi usado o compilador **g++** e para compilar o projéto são usadas as seguintes linhas de compilação.

1. test-ball

    ```
    $ g++ test-ball.cpp ball.cpp ball.h -lGL -lGLU -lglut -o test-ball 
    ```

2. test-ball-graphics

    ```
    $ g++ test-ball-graphics.cpp graphics.cpp ball.cpp -lGL -lGLU -lglut -o test-ball-graphics
    ```

3. test-springmass

    ```
    $ g++ test-springmass.cpp springmass.cpp  -lGL -lGLU -lglut -o test-springmass
    ```

4. test-springmass-graphics

    ```
    $ g++ test-springmass-graphics.cpp graphics.cpp springmass.cpp  -lGL -lGLU -lglut -o test-springmass-graphics
    ```

>Obs.: para compilar o test-ball, foi incluido na linha de compilação o arquivo ball.h, não é necessária a inclusão dele na compilação, porem isso foi feito para que, caso modificado, o makefile ira compilá-lo também.

### Arquivos

1. Makefile :
    Contém os comandos de compilação dos programas do projéto.

2. ball.cpp :
    Define os métodos da classe **Ball**.

3. ball.h :
    Declara a classe **Ball**.

4. graphics.cpp :
    Define os métodos das classes **Drawable** (Abstrata) e **Figure** (hrerdeira de Drawable), e define a função *run*.

5. graphics.h :
    Declara a função *run* e as classes **Drawable** e **Figure**.

6. plot-ball.R :
    Um script da lingugem **R** para plotar o gráfico da posição da Bola durante a simulação. 

7. simulation.h :
    Declara a classe abstrata **Simulation**, ela serve de base para a classe **Ball**.

8. springmass.cpp :
    Define os métodos das classes **Mass**, **Spring** e **SpringMass**.

9. springmass.h :
    Declara as classes **Vector2**, **Mass**, **Spring** e **SpringMass**, e também declara e define os operadores **+**, **-**, **\*** e **\\** de **Vector2**.

10. test-ball-graphics.cpp :
    Declara e define os métodos da classe **BallDrawable** (herdeira de **Ball** e **Drawable**), e define uma **main**, essa, chama os comandos que mostram a simulação na tela.

11. test-ball.cpp :
    Define uma **main** e a função **run**, que rodam uma simulação que somente imprime, na linha de comando, as posições da bola.

12. test-springmass-graphics.cpp :
    Declara e define a classe **SpringMassDrawable** e uma **main** que roda a simulação.

13. test-springmass.cpp :
    Declara e define uma **main** roda a simulação e a imprime na linha de comando.

### Diagrama de Classes

![alt diagrama](diagrama.png)

>Obs.:a classe Simulation é puramente abstrata, todos os seus metodos são virtuais, e a pesar de não parecer tanto o nome da classe quanto os métodos estão em itálico.

### Gráfico

Para melhor visualízarmos os nossos objetivos com esse simulador fizemos um gráfico com a posição da bola em alguns instantes.

![alt plot](plot.png)

### Como foi feito o Gráfico

O gráfico acima foi feito usando a linguge de programação **R**, o script para gerar o gráfico está no arquivo **plot-ball.R**, mas também pode ser gerado manualmente atravéz dos comandos abaixo.

Primeiro geramos e salvamos os pontos.
```
$ ./test-ball > dados
```

Apos isso abrimos o R e lemos os dados
``` R
dados <- read.table("dados", header = FALSE, sep = " ", col.names = c("x", "y"))
```

E então plotamos o gráfico
``` R
plot(dados$x, dados$y, xlab = "X", ylab = "Y")
```

Após isso basta salvar o gráfico

Para usar o script pode ser usado o comando abaixo
```
$ R < plot-ball.R --no-save
```
